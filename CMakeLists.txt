cmake_minimum_required(VERSION 3.5)

project(asio_vendor)

find_package(ament_cmake REQUIRED)

set(PACKAGE_VERSION "1.0.0")

macro(build_asio)
    
  include(ExternalProject)
  ExternalProject_Add(asio
    GIT_REPOSITORY https://github.com/chriskohlhoff/asio.git
    GIT_TAG asio-1-18-0
    TIMEOUT 600
    CONFIGURE_COMMAND ""
    BUILD_COMMAND ""
    INSTALL_COMMAND ""
  )

  # The external project will install to the build folder, but we'll install that on make install.
  install(
    DIRECTORY
      ${CMAKE_CURRENT_BINARY_DIR}/asio-prefix/src/asio/asio/include/
    DESTINATION
      ${CMAKE_INSTALL_PREFIX}/include
    PATTERN "Makefile.am" EXCLUDE
    PATTERN ".*" EXCLUDE
  )

endmacro()

find_package(asio QUIET)
if(NOT asio_FOUND)
  build_asio()
endif()

ament_package()
